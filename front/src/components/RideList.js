import axios from "axios";
import { useEffect, useState } from "react";
import { RideItem } from "./RideItem";
import { baseUrl } from "../constants";
import styled from "styled-components";


const MainContainer = styled.div`
    margin: auto;
    width: fit-content;
    background-color: white;
    height: 100%;
    border-radius: 25px;
    padding-bottom: 20px;
    margin-top: 80px;
    box-shadow: rgba(17, 17, 26, 0.05) 0px 1px 0px, rgba(17, 17, 26, 0.1) 0px 0px 8px;
`;

const MainTitle = styled.h1`
    padding: 25px;
    border-bottom: 1px #e89999c0 solid;
    text-align: center;
`;

const Container = styled.div`
    height:700px;
    overflow:scroll;
    overflow-x:hidden;
    scroll-behavior: smooth;
`;


export const RideList = () => {
    const [rides, setRides] = useState([])
    
    useEffect(() => {
        axios.get(`${baseUrl}/rides/`).then(res => {
            setRides(res.data)
        })
    }, [])

    return (
        <MainContainer>
            <MainTitle>RIDES LIST</MainTitle>
            <Container>
                {rides.map((ride, idx) =>
                    <>
                        <RideItem ride={ride} key={idx}/>
                        {idx !== rides.length-1 }
                    </>
                )}
            </Container>
        </MainContainer>
    )
}