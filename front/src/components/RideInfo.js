import styled from "styled-components";

const InfoItem = styled.div`
    margin-right: 35px;
`;

export const Info = ({ title, value }) => {
    return (
        <InfoItem>
            <span style={{ marginRight: '5px' }}>{title}</span>
                <span>{value}</span>
        </InfoItem>
            )
}