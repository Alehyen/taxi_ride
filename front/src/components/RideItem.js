import { useEffect, useState } from "react";
import { baseUrl } from "../constants";
import axios from "axios";
import styled from "styled-components";
import { Info } from "./RideInfo";
import { GiPathDistance } from "react-icons/gi";
import { GiSandsOfTime } from "react-icons/gi";
import { GiReceiveMoney } from "react-icons/gi";
import Drawer from 'react-drag-drawer'


const Item = styled.div`
    display:block;
    padding: 10px 40px;
    cursor: pointer;
`;

const Container = styled.div`
    padding: 16px;
    padding-bottom: 24px;
    border-radius: 20px;
    background-color: ${({ distance }) => distance > 2 ? 'rgba(240,50,50,0.2)' : '#ededed83'};
`;

const Alert = styled.div`
    background-color: white;
    padding: 120px;
    border-radius 25px;
    padding-bottom: 30px
`;


export const RideItem = ({ ride }) => {
    const [price, setPrice] = useState()
    const [clicked, setClicked] = useState(false)
    const [open, setopen] = useState(false)

    useEffect(() => {
        axios.get(`${baseUrl}/rides/${ride.id_ride}/price/`).then(res => {
            setPrice(res?.data?.price)
        })
    }, [ride.id_ride])

    const finishDate = new Date(ride.start_time)
    finishDate.setSeconds(finishDate.getSeconds() + ride.duration)

    const handleClick = () => {
        setopen(true);
        setClicked(true);
    }

    return (
        <Item onClick={handleClick}>
            <Drawer
                open={open}
                onRequestClose={() => setopen(false)}
            >
                <Alert>
                    <h4>
                        {new Date(ride.duration * 1000).toISOString().substr(11, 8)} - {finishDate.toISOString()}
                    </h4>
                    <button onClick={() => setopen(false)} style={{ marginLeft: 125, marginTop: 40 }}>close</button>
                </Alert>
            </Drawer>
            <Container distance={ride.distance}>
                <h4>Ride {ride.id_ride} {clicked ? '(cliked)' : ''}</h4>
                <div style={{ display: "flex" }}>
                    <GiPathDistance style={{ padding: 2, paddingRight: 8 }} />
                    <Info title='Distance:' value={`${ride.distance} mile${ride.distance > 1 ? 's' : ''}`} />
                    <GiSandsOfTime style={{ padding: 2, paddingRight: 8 }} />
                    <Info title='start:' value={new Date(ride.start_time).toISOString()} />
                    <GiReceiveMoney style={{ padding: 2, paddingRight: 8 }} />
                    <Info title='price:' value={`${price} €`} />
                </div>
            </Container>
        </Item>

    )
}