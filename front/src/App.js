import { RideList } from './components/RideList';

import './App.css';


function App() {
  return (
    <div className="App">
      <RideList />
    </div>
  );
}

export default App;
