FROM python:3.8.3-slim
WORKDIR /home/docker/code

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install necessary dependencies
RUN apt-get update \
    && apt-get install -y \
    build-essential \
    libpq-dev \
    libssl-dev \
    libffi-dev \
    python3-dev \
    netcat

RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt
RUN pip install gunicorn
RUN pip install psycopg2

# Collect static files
RUN mkdir -p /home/docker/staticfiles

# copy project
COPY . .

RUN ["chmod", "+x", "/home/docker/code/entrypoint.sh"]
ENTRYPOINT ["./entrypoint.sh"]
