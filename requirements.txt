Django==3.2.8
djangorestframework==3.12.4
django-cors-headers==3.10.0
pytest-django==4.4.0
psycopg2==2.9.1