POSTGRES_CONTAINER ?= postgres-taxi-rides
POSTGRES_IMAGE ?= postgres:13
DIVE_MEDICAL_DB ?= rides
POSTGRES_PASSWORD ?= azerty789
POSTGRES_BIND_PORT ?= 5432
BIND_PORT ?= -p ${POSTGRES_BIND_PORT}:5432

run_postgres_local:
	docker run -d \
	--name ${POSTGRES_CONTAINER} \
	-e POSTGRES_PASSWORD=${POSTGRES_PASSWORD} \
	-e POSTGRES_DB=${DIVE_MEDICAL_DB} \
	${BIND_PORT} \
	${POSTGRES_IMAGE}

run_local:
	python3 -m venv venv
	./venv/bin/python3 -m pip install -r requirements.txt
	./venv/bin/python3 ./manage.py migrate
	./venv/bin/python3 ./manage.py insert_rides
	./venv/bin/python3 ./manage.py runserver 0.0.0.0:8000

run: run_postgres_local run_local

destroy_postgres:
	docker rm -f ${POSTGRES_CONTAINER} || true

clean:
	rm -rf venv