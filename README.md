# Taxi Ride coding test
Hello there! this my implementation of the Paris Taxi Fare coding assignment

I had fun doing it, and please feel free to reach out if you have any further questions!

DEMO

![](ALEHYEN_ISMAIL_TAXI_RIDE_DEMO.mp4)
## Backend
The backend is a Django REST API created with django rest framework. Endpoints:

- GET /api/rides/ : list all rides
- POST /api/rides/ : create a new ride, body should be a JSON in the following format:
```buildoutcfg
{
    "start_time": "2021-10-11T17:01:17.031Z",
    "distance": 4,
    "duration": 4500
}
```
- PUT /api/rides/<id_ride>/: modify a ride, same body as in POST
- DELETE /api/rides/<id_ride>/: delete a ride
- GET /api/rides/<id_ride>/price/: get the price of a ride

to start the backend, make sure you are in the folder `taxi_ride` and then `make run`,
this will create a docker container for the Postgres database, create a virtual environment to install requirements,
insert some rides in the database and finally start the server

PS: please make sure you have docker installed

A docker compose file is attached to simulate the deployment of the coding challenge
`docker-compose up -d --build`. I used Nginx as a web server to serve the front and the api with Gunicor as
a wsgi server. You should be able to see the app up and running at `http://localhost:80`

### Tests
I used pytest to test the different parts of the api, especially the pricing algorithm since it is the
core proprietary intellectual property of the company

to run the tests make sure you are in the folder `taxi_ride` and then `pytest`

## Front
the front is a React app created with `create-react-app`

to start the front make sure you are in the `taxi_ride/front` folder and then `npm install` `npm start`