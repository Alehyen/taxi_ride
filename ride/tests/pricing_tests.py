import pytest
from datetime import datetime

from ride import pricing_service


@pytest.mark.parametrize(
    'start_time, price',
    [
        (datetime.strptime('2021-10-11T13:01:17.031Z', '%Y-%m-%dT%H:%M:%S.%fZ'), 0.5),
        (datetime.strptime('2021-10-11T17:01:17.031Z', '%Y-%m-%dT%H:%M:%S.%fZ'), 1.5),
        (datetime.strptime('2021-10-11T19:01:17.031Z', '%Y-%m-%dT%H:%M:%S.%fZ'), 0.5),
        (datetime.strptime('2021-10-11T21:01:17.031Z', '%Y-%m-%dT%H:%M:%S.%fZ'), 1),
        (datetime.strptime('2021-10-11T00:01:17.031Z', '%Y-%m-%dT%H:%M:%S.%fZ'), 1),
        (datetime.strptime('2021-10-11T5:01:17.031Z', '%Y-%m-%dT%H:%M:%S.%fZ'), 1),
        (datetime.strptime('2021-10-11T6:01:17.031Z', '%Y-%m-%dT%H:%M:%S.%fZ'), 0.5)
    ]
)
def test_get_price_per_one_fifth_mile(start_time, price):
    assert pricing_service.get_price_per_one_fifth_mile(start_time) == price


def test_calculate_ride_price(rides):
    r1, r2, r3 = rides
    assert pricing_service.calculate_ride_price(r1) == 6
    assert pricing_service.calculate_ride_price(r2) == 23.5
    assert pricing_service.calculate_ride_price(r3) == 26


def test_pricing_endpoint(rides, api_client):
    for ride in rides:
        res = api_client.get(f'/api/rides/{ride.id_ride}/price/')
        assert res.status_code == 200
        assert res.data['price'] == pricing_service.calculate_ride_price(ride)


@pytest.mark.django_db
def test_pricing_ride_does_exist(api_client):
    res = api_client.get('/api/rides/100/price/')
    assert res.status_code == 404
    assert res.data['detail'] == 'Ride does not exist'
