import pytest
from datetime import datetime
from rest_framework.test import APIClient
from ride import models


@pytest.fixture
def rides(db):
    r1 = models.Ride.objects.create(
        start_time=datetime.strptime('2021-10-11T13:01:17+00:00', '%Y-%m-%dT%H:%M:%S%z'),
        distance=2,
        duration=5000
    )
    r2 = models.Ride.objects.create(
        start_time=datetime.strptime('2021-10-11T17:01:17+00:00', '%Y-%m-%dT%H:%M:%S%z'),
        distance=3,
        duration=7000
    )
    r3 = models.Ride.objects.create(
        start_time=datetime.strptime('2021-10-11T01:05:17+00:00', '%Y-%m-%dT%H:%M:%S%z'),
        distance=5,
        duration=9000
    )

    return r1, r2, r3


@pytest.fixture
def api_client():
    return APIClient()
