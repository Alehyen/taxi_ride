from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response

from ride import serializers, models
from ride import pricing_service
from ride import exceptions


class RideViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.RideSerializer
    queryset = models.Ride.objects.all()


class PricingView(APIView):
    def get(self, request, id_ride):
        try:
            ride = models.Ride.objects.get(id_ride=id_ride)
        except models.Ride.DoesNotExist:
            raise exceptions.RideDoesNotExist
        price = pricing_service.calculate_ride_price(ride)
        return Response({
            "price": price
        })

