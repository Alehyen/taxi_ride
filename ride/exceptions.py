from rest_framework.exceptions import APIException
from rest_framework import status


class RideDoesNotExist(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = 'Ride does not exist'
    default_code = 'ride_does_not_exist'
