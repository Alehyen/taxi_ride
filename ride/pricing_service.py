from ride import models


def get_price_per_one_fifth_mile(start_time):
    initial = 0.5
    start_hour = start_time.hour
    if 20 <= start_hour <= 23 or 0 <= start_hour < 6:
        initial += 0.5
    if 16 <= start_hour < 19:
        initial += 1
    return initial


def calculate_ride_price(ride: models.Ride):
    initial_price = 1
    price_per_fifth_mile = get_price_per_one_fifth_mile(ride.start_time)
    return initial_price + ((ride.distance / (1/5)) * price_per_fifth_mile)
