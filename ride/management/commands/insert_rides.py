from django.core.management.base import BaseCommand
from ride.models import Ride


class Command(BaseCommand):
    help = "insert some fake rides"
    requires_system_checks = False

    def handle(self, *args, **options):
        rides = [
            {
                "distance": 2,
                "start_time": "2020-06-19T13:01:17.031Z",
                "duration": 9000
            },
            {

                "distance": 1,
                "start_time": "2020-06-19T12:01:17.031Z",
                "duration": 6000
            },
            {

                "distance": 5,
                "start_time": "2020-06-19T14:01:17.031Z",
                "duration": 7000
            },
            {

                "distance": 5,
                "start_time": "2020-06-19T14:11:17.031Z",
                "duration": 4000
            }
        ]

        for item in rides:
            ride = Ride(**item)
            ride.save()

