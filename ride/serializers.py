from rest_framework import serializers
from ride import models


class RideSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Ride
        fields = "__all__"