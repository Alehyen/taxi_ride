from django.db import models


class Ride(models.Model):
    id_ride = models.AutoField(primary_key=True)
    distance = models.FloatField()
    start_time = models.DateTimeField()
    duration = models.FloatField()
